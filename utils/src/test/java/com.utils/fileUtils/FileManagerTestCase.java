package com.utils.fileUtils;

import com.utils.UtilsConfig;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileManagerTestCase {
    private static Logger logger = Logger.getLogger(FileManagerTestCase.class);
    private static final String pathToTmpDir = "target/test-classes/tmpForFileUtilsTestCase/";
    private static final String fileDestination = pathToTmpDir + "/test.txt";
    private static final String urlToTestFile = UtilsConfig.getUrlToTxt();

    @Test
    public void testCopyUrlToFile() {
        File testFile = new File(fileDestination);
        logger.info("Copy file from URL: " + urlToTestFile + "to directory" + testFile.getAbsolutePath());
        FileManager.copyUrlToFile(urlToTestFile, fileDestination);
        logger.info("Check that file " + testFile.getName() + " was successfully copied to " + testFile.getAbsolutePath());
        Assert.assertTrue(new File(fileDestination).exists());
    }

    @Test
    public void testCleanDirectory() {
        FileManager.copyUrlToFile(urlToTestFile, fileDestination);
        FileManager.cleanDirectory(pathToTmpDir);
        boolean filesPresence = false;
        try {
            filesPresence = Files.list(Paths.get(pathToTmpDir)).findAny().isPresent();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(filesPresence);
    }
}
