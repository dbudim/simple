package com.utils.testNG;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParallelTestCase extends BaseTestNG {
    private static Logger logger = Logger.getLogger( ParallelTestCase.class );

    @Test (dataProvider = "provider", threadPoolSize = 20)
    public void test1(String parameter) {
        logger.info("THREAD "  + Thread.currentThread().getId() + " test data " + parameter );
    }

    @DataProvider(parallel = true)
    private Object[][] provider(){
        return new Object[][]{
                {"OBJECT 1"},
                {"OBJECT 2"},
                {"OBJECT 3"},
                {"OBJECT 4"},
                {"OBJECT 5"},
                {"OBJECT 6"},
                {"OBJECT 7"},
                {"OBJECT 8"},
                {"OBJECT 9"},
                {"OBJECT 10"},
                {"OBJECT 11"},
                {"OBJECT 12"},
                {"OBJECT 13"},
                {"OBJECT 14"},
                {"OBJECT 15"},
                {"OBJECT 16"},
                {"OBJECT 17"},
                {"OBJECT 18"},
                {"OBJECT 19"},
                {"OBJECT 20"}
        };
    }
}
