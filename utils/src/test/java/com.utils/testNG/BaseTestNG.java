package com.utils.testNG;

import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTestNG {
    private static Logger logger = Logger.getLogger(BaseTestNG.class);

    @BeforeClass
    public void beforeClass() {
        logger.info("Start Before Class...");
    }

    @BeforeMethod
    public void beforeMethod() {
        logger.info("Start Before Method...");
    }

    @AfterMethod()
    public void afterMethod(ITestResult iTestResult) {
        if ( !iTestResult.isSuccess()){
            logger.info("TEST IS FAILED.... PRINTED VIA ITESTRESULT");
        }
        logger.info("Do After Method...");
    }

    @AfterClass
    public void afterClass() {
        logger.info("Do After Class");
    }
}
