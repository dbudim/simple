package com.utils.testNG;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestNGTestCase extends BaseTestNG {
    private static Logger logger = Logger.getLogger(TestNGTestCase.class);
    private static final File TEST_FILE = new File(TestNGTestCase.class.getClassLoader().getResource("testData/testData.csv").getFile());

    @Test
    public void test1() {
        logger.info("Execute test1");
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void test2() {
        logger.info("Execute test2");
        List<String> nullList = null;
        nullList.size();
    }

    @Test(timeOut = 10, enabled = false)
    public void test3() {
        logger.info("Execute test3");
        try {
            Thread.sleep(11);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test(groups = "unit")
    public void test4() {
        logger.info("Execute test4");
    }

    @Test(groups = "unit")
    public void test5() {
        logger.info("Execute test5");
    }

    @Test(dependsOnMethods = "test1")
    public void test6() {
        logger.info("Execute test6");
    }

    @Test
    @Parameters("parameter")
    public void test7(@Optional("default value") String parameter) {
        logger.info("Execute test7");
        logger.info("Parameter value: " + parameter);
    }

    @Test(dataProvider = "parameters", groups = "provider")
    public void test8(String parameter) {
        logger.info("Execute test8");
        logger.info("Parameter value: " + parameter);
    }

    @Test
    public void test9() {
        logger.info("Execute test9");
    }

    @Test(dataProvider = "provideData", groups = "provider")
    public void test10(String country, String city, String street) {
        logger.info("Execute test 10");
        logger.info("Params from file: " + country + " " + city + " " + street);
    }

    @DataProvider
    private Object[][] parameters() {
        return new Object[][]{
                {"param1"},
                {"param2"},
                {"param3"}
        };
    }

    @DataProvider
    private Iterator<Object[]> provideData() {
        List<Object[]> data = null;
        try {
            data = getDataFromCsv(TEST_FILE);
        } catch (IOException e) {
            logger.error("Can't get data from file: " + TEST_FILE.getName());
        }
        return data.iterator();
    }

    private List<Object[]> getDataFromCsv(File file) throws IOException {
        List<Object[]> data = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = reader.readLine()) != null) {
            data.add(line.split(","));
        }
        return data;
    }
}
