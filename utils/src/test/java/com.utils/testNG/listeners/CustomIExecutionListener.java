package com.utils.testNG.listeners;

import org.testng.IExecutionListener;

public class CustomIExecutionListener implements IExecutionListener {
    @Override
    public void onExecutionStart() {
        System.out.println("TEXT PRINTED VIA \"IExecutionListener\" LISTENER ON STARTING");
    }

    @Override
    public void onExecutionFinish() {
        System.out.println("TEXT PRINTED VIA \"IExecutionListener\" LISTENER ON FINISHING");
    }
}
