package com.utils;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class ConfigTestCase {
    private static final String CONFIG_NAME = UtilsConfig.getUtilsConfigName();
    private static Logger logger = Logger.getLogger(ConfigTestCase.class);

    @Test
    public void testConfigPresent() {
        File config = new File(UtilsConfig.class.getClassLoader().getResource(CONFIG_NAME).getFile());
        logger.info("Check that config file is present by pathToUtilsConfig: " + config.getAbsolutePath());
        Assert.assertTrue(config.exists());
    }

    @Test
    public void testConfigConsistProperties() {
        Properties properties = UtilsConfig.loadProperties(CONFIG_NAME);
        Set<String> actualProperties = properties.stringPropertyNames();
        Set<String> expectedProperties = new HashSet<>(Arrays.asList(
                "URL_TO_JSON",
                "URL_TO_PDF",
                "URL_TO_TXT",
                "URL_TO_XML",
                "URL_TO_XLS"));
        logger.info("Check that config file: " + UtilsConfig.getUtilsConfigName() + " contains all properties");
        assertEquals(actualProperties, expectedProperties);
    }

    @Test
    public void testConfigPropertiesNotEmpty() {
        Properties properties = UtilsConfig.loadProperties(CONFIG_NAME);
        Set<String> propertiesFromFile = properties.stringPropertyNames();
        logger.info("Check that all properties are defined");
        Assert.assertTrue(propertiesFromFile.stream()
                .filter(property -> properties.getProperty(property)
                        .isEmpty())
                .collect(Collectors.toList()).size() == 0);
    }
}
