//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.utils.fileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class FileManager {
    private static Logger logger = Logger.getLogger(FileManager.class);

    public FileManager() {
    }

    public static void cleanDirectory(String pathToDirectory) {
        try {
            logger.info("Clean directory: " + pathToDirectory);
            FileUtils.cleanDirectory(new File(pathToDirectory));
        } catch (IOException var2) {
            logger.error("Can't clean directory!");
            var2.printStackTrace();
        }
    }

    public static void copyUrlToFile(String url, String destination) {
        try {
            logger.info("Try to create file:" + destination);
            FileUtils.copyURLToFile(new URL(url), new File(destination));
        } catch (IOException var3) {
            logger.error("Cant create file!");
            var3.printStackTrace();
        }

    }
}
