package com.utils.httpUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;

public class RequestExecutor {

    private static Logger logger = Logger.getLogger( RequestExecutor.class );

    public HttpResponse executeGetAndGetResponse( String url ) {
        HttpClient httpClient = HttpClients.createDefault();
        HttpResponse httpResponse = null;
        try {
            logger.info( "Execute GET request: " + url );
            httpResponse = httpClient.execute( new HttpGet( url ) );
        } catch ( IOException e ) {
            logger.error( "Can't execute GET request: " + url );
            e.printStackTrace();
        }
        return httpResponse;
    }

    public String getStringJson( String url ) {
        HttpResponse httpResponse;
        String json = null;
        try {
            logger.info( "Extracting String Json..." );
            httpResponse = executeGetAndGetResponse( url );
            json = EntityUtils.toString( httpResponse.getEntity() );
        } catch ( IOException e ) {
            logger.error( "Can't extract String from Json!" );
            e.printStackTrace();
        }
        return json;
    }
}
