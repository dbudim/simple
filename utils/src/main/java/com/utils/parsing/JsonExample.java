package com.utils.parsing;

public class JsonExample {
    private String title;

    private String WhoAmI;

    private String ID;

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getWhoAmI() {
        return WhoAmI;
    }

    public void setWhoAmI( String WhoAmI ) {
        this.WhoAmI = WhoAmI;
    }

    public String getID() {
        return ID;
    }

    public void setID( String ID ) {
        this.ID = ID;
    }
}
