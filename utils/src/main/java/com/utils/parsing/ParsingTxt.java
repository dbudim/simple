package com.utils.parsing;

import com.utils.UtilsConfig;
import com.utils.parcer.Parser;
import com.utils.parcer.TxtParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

public class ParsingTxt {
    private static Logger logger = Logger.getLogger( ParsingPdf.class );
    private static final String PATH_TO_TXT = UtilsConfig.getPathToFiles() + "example.txt";

    public static void main( String[] args ) {
        Parser txtParser = new TxtParser();
        logger.info( "Parse and print TXT file from URL: " + UtilsConfig.getUrlToPdf() );
        List<String> parsedPdfUrl = txtParser.parseFileByUrl( UtilsConfig.getUrlToTxt() );
        parsedPdfUrl.forEach( System.out::println );

        logger.info( "Parse and print TXT file from local storage: " + new File( PATH_TO_TXT ).getAbsolutePath() );
        List<String> parsedPdfFile = txtParser.parseFileByPath( PATH_TO_TXT );
        parsedPdfFile.stream().forEach( System.out::println );
    }
}
