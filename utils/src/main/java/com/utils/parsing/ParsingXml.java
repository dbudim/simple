package com.utils.parsing;

import com.utils.UtilsConfig;
import com.utils.customParser.XmlCustomParser;

public class ParsingXml {
    public static void main(String[] args) {

        XmlCustomParser xmlCustomParser = new XmlCustomParser();
        xmlCustomParser.parseXmlByTagName( UtilsConfig.getUrlToXml(), "TITLE")
                .stream()
                .forEach(System.out::println);

    }
}
