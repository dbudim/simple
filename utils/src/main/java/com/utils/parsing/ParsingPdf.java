package com.utils.parsing;

import com.utils.UtilsConfig;
import com.utils.parcer.Parser;
import com.utils.parcer.PdfParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

public class ParsingPdf {
    private static Logger logger = Logger.getLogger( ParsingPdf.class );
    private static final String PATH_TO_PDF = UtilsConfig.getPathToFiles() + "example.pdf";

    public static void main( String[] args ) {
        Parser pdfParser = new PdfParser();

        logger.info( "Parse and print PDF file from URL: " + UtilsConfig.getUrlToPdf() );
        List<String> parsedPdfUrl = pdfParser.parseFileByUrl( UtilsConfig.getUrlToPdf() );
        parsedPdfUrl.forEach( System.out::println );

        logger.info( "Parse and print PDF file from local storage: " + new File( PATH_TO_PDF ).getAbsolutePath() );
        List<String> parsedPdfFile = pdfParser.parseFileByPath( PATH_TO_PDF );
        parsedPdfFile.stream().forEach( System.out::println );
    }
}
