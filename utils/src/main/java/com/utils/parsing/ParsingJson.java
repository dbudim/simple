package com.utils.parsing;

import com.google.gson.Gson;
import com.utils.UtilsConfig;
import com.utils.customParser.JsonCustomParser;
import com.utils.httpUtils.RequestExecutor;

import java.util.stream.Stream;

public class ParsingJson {

    public static void main( String[] args ) {
        JsonCustomParser jsonCustomParser = new JsonCustomParser();
        String json = new RequestExecutor().getStringJson( UtilsConfig.getUrlToJson() );
        jsonCustomParser.parse( json )
                        .entrySet()
                        .stream()
                        .forEach( System.out::println );


        Gson gson = new Gson();
        JsonExample2 jsonExample2 = gson.fromJson(
                new RequestExecutor().getStringJson( UtilsConfig.getUrlToJson() ),
                JsonExample2.class
        );
        Stream.of( jsonExample2.getId(),
                   jsonExample2.getUserId(),
                   jsonExample2.getBody(),
                   jsonExample2.getTitle() ).forEach( System.out::println );
    }


}
