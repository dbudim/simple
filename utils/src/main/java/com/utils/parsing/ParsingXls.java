package com.utils.parsing;

import com.utils.UtilsConfig;
import com.utils.parcer.XlsParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class ParsingXls {
    private static Logger logger = Logger.getLogger(ParsingPdf.class);
    private static final String PATH_TO_XLS = UtilsConfig.getPathToFiles() + "example.xls";

    public static void main(String[] args) {
        XlsParser xlsParser = new XlsParser();
        logger.info("Parse and print XLS file from URL: " + UtilsConfig.getUrlToXls());
        List<String> parsedXlsUrl = xlsParser.parseFileByUrl(UtilsConfig.getUrlToXls());
        parsedXlsUrl.forEach(System.out::println);

        logger.info("Parse and print XLS file from local storage: " + new File(PATH_TO_XLS).getAbsolutePath());
        List<String> parsedPdfFile = xlsParser.parseFileByPath(PATH_TO_XLS);
        parsedPdfFile.stream().forEach(System.out::println);

        logger.info("Find column \"Report\" and get URLs");
        int targetColumnIndex = xlsParser.getIndexOfColumnByName(UtilsConfig.getUrlToXls(), "Report title");
        List<String> urlsFromColumn = xlsParser.getColumnValuesFromDocumnet(UtilsConfig.getUrlToXls(), targetColumnIndex)
                .stream()
                .filter(cell -> cell.contains("http"))
                .collect(Collectors.toList());

        urlsFromColumn.stream().forEach(System.out::println);
    }

}
