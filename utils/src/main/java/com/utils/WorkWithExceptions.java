package com.utils;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class WorkWithExceptions {
    private static Logger logger = Logger.getLogger( WorkWithExceptions.class );

    public static void main( String[] args ) {

    }

    private static void withNullPointerException() {
        String cat = null;
        cat.split( "a" );
    }

    private static void withNumberFormatException() {
        String word = "some string";
        int number = Integer.valueOf( word );
    }

    private static void withIllegalArgumentException( int argument ) {
        if ( argument > 0 ) {
            throw new IllegalArgumentException( "Argument is > 0" );
        }
    }

    private static void withillegalStateException() {
        Thread thread = new Thread();
        thread.start();
        thread.start();
    }

    private static void withClassCastExceptions() {
        Object object = 1;
        String string = (String) object;
    }

    private static void withParseException() throws ParseException {
        String date = "ololo";
        DateFormat dateFormat = new SimpleDateFormat( "yyyy-mm-dd" );
        dateFormat.parse( date );
    }
}
