package com.utils.customParser;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class XmlCustomParser {

    Logger logger = Logger.getLogger(XmlCustomParser.class);

    public List<String> parseXmlByTagName( String url, String tag) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("Can't create new documentBuilder");
            e.printStackTrace();
            System.exit( 1 );
        }
        Document document = null;
        try {
            document = documentBuilder.parse(new URL(url).openStream());
        } catch (SAXException e) {
            e.printStackTrace();
            System.exit( 1 );
        } catch (IOException e) {
            logger.error("Can't get resourse by URL: " + url);
            e.printStackTrace();
            System.exit( 1 );
        }
        NodeList nodeList = document.getElementsByTagName(tag);
        List<String> stringNodes = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            stringNodes.add(nodeList.item(i).getTextContent());
        }
        return stringNodes;
    }
}
