package com.utils.customParser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class JsonCustomParser {

    private static Logger logger = Logger.getLogger(JsonCustomParser.class);

    public Map<String, String> parse(String json) {
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(json);
        logger.info("Parsing Json structure...");
        Set<Map.Entry<String, JsonElement>> entriesData = jsonObject.entrySet();
        return entriesData.stream().collect(Collectors.toMap(
                entry -> entry.getKey(),
                entry -> entry.getValue().toString()
        ));
    }

    public List<String> getStringListFromBigJson(String json) {
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonObjects = jsonParser.parse(json).getAsJsonArray();
        List<String> stringJsonObjects = new ArrayList<>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            stringJsonObjects.add(jsonObjects.get(i).toString());
        }
        return stringJsonObjects;
    }
}
