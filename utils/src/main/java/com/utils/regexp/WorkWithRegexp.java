package com.utils.regexp;

import com.utils.UtilsConfig;
import com.utils.parcer.XlsParser;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class WorkWithRegexp {
    private static Logger logger = Logger.getLogger( WorkWithRegexp.class );

    public static void main( String[] args ) {

        List<String> input = new XlsParser().parseFileByUrl( UtilsConfig.getUrlToXls() );

        String rbrLinkRegexp = "https://app-[a-z]+[.][a-z]+[.]com/rbr/.+";
        Pattern pattern = Pattern.compile( rbrLinkRegexp );

        List<String> rbrLinks = new ArrayList<>();
        for ( String in : input ) {
            if ( pattern.matcher( in ).matches() ) {
                rbrLinks.add( in );
                logger.info( in + " matches to regexp" );
            }
        }


    }
}
