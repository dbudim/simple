package com.utils.regexp;

import com.utils.parcer.TxtParser;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetParamsFromUrlByMatcher {
    private static Logger logger = Logger.getLogger(GetParamsFromUrlByMatcher.class);
    private static final String URL_TO_TXT = "http://www.ssstest.com/test/AntonPilipenko/matcher.txt";

    public static void main(String[] args) {
        String rowWithParams = new TxtParser().parseFileByUrl(URL_TO_TXT).get(0);
        Pattern pattern = Pattern.compile("(parameter[\\d]*[=][\"])((\\w+=)?([A-Z][\\d]+[.][A-Z][\\d]))([\"])");
        Matcher matcher = pattern.matcher(rowWithParams);

        List<String> paramValues = new ArrayList<>();
        logger.info("Get parameter values from url...");
        while (matcher.find()) {
            logger.info(matcher.group(2) + " value matches to regexp");
            paramValues.add(matcher.group(2));
        }
    }
}
