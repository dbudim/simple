package com.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class UtilsConfig {
    private static String URL_TO_PDF;
    private static String URL_TO_XLS;
    private static String URL_TO_JSON;
    private static String URL_TO_TXT;
    private static String URL_TO_XML;
    private static Properties UTILS_PROPERTIES;
    private static final String UTILS_CONFIG_NAME = "utilsConfig.properties";
    private static final String PATH_TO_FILES = "target/classes/files/";
    private static Logger logger = Logger.getLogger(UtilsConfig.class);

    static {
        UTILS_PROPERTIES = loadProperties(UTILS_CONFIG_NAME);
        URL_TO_PDF = UTILS_PROPERTIES.getProperty("URL_TO_PDF");
        URL_TO_XLS = UTILS_PROPERTIES.getProperty("URL_TO_XLS");
        URL_TO_JSON = UTILS_PROPERTIES.getProperty("URL_TO_JSON");
        URL_TO_TXT = UTILS_PROPERTIES.getProperty("URL_TO_TXT");
        URL_TO_XML = UTILS_PROPERTIES.getProperty("URL_TO_XML");
    }

    public static Properties loadProperties(String configName) {
        Properties properties = new Properties();
        try {
            logger.info("Loading config UTILS_PROPERTIES...");
            properties.load(UtilsConfig.class.getClassLoader().getResourceAsStream(configName));
            logger.info("Success!");
        } catch (IOException e) {
            logger.error("Can't load: " + configName);
            e.printStackTrace();
        }
        return properties;
    }

    public static String getUrlToPdf() {
        return URL_TO_PDF;
    }

    public static String getUrlToXls() {
        return URL_TO_XLS;
    }

    public static String getUrlToJson() {
        return URL_TO_JSON;
    }

    public static String getUrlToTxt() {
        return URL_TO_TXT;
    }

    public static String getUrlToXml() {
        return URL_TO_XML;
    }

    public static String getPathToFiles() {
        return PATH_TO_FILES;
    }

    public static String getUtilsConfigName() {
        return UTILS_CONFIG_NAME;
    }
}

