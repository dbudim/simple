package com.utils;

import com.utils.httpUtils.RequestExecutor;
import org.apache.log4j.Logger;

public class WorkWithTernary {
    private static Logger logger = Logger.getLogger( WorkWithTernary.class );

    public static void main( String[] args ) {

        checkRequestSuccess( new RequestExecutor().executeGetAndGetResponse( UtilsConfig.getUrlToJson() )
                                                  .getStatusLine()
                                                  .getStatusCode() );
        checkStringElementsEquals( "jkj", "jkj" );
    }

    /**
     * Execute request and log if result Success or Fail
     */
    private static void checkRequestSuccess( int statusCode ) {
        String result = statusCode == 200 ? "Success" : "Fail";
        logger.info( "Status code is " + statusCode + " request is " + result );
    }

    /**
     * Ternary with boolean. Check if two strings are equals and return true or false
     */

    private static boolean checkStringElementsEquals( String element1, String element2 ) {
        logger.info( "Check that \"" + element1 + "\" equls \"" + element2 + "\"" );
        boolean result = element1.equals( element2 ) ? true : false;
        logger.info( "Result is " + result );
        return result;
    }
}

