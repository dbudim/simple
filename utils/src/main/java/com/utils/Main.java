package com.utils;

import com.utils.parcer.Parser;
import com.utils.parcer.parserBuilder.ParserBuilder;
import com.utils.parcer.parserBuilder.PdfParserBuilder;
import org.apache.log4j.Logger;

public class Main {
    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        ParserBuilder parserBuilder = new PdfParserBuilder();
        Parser parser = parserBuilder.createParser();
        parser.parseFileByUrl(UtilsConfig.getUrlToPdf()).stream().forEach(System.out::println);
    }
}
