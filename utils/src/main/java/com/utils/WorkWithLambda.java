package com.utils;

import com.utils.parcer.PdfParser;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class WorkWithLambda {
    private static Logger logger = Logger.getLogger(WorkWithLambda.class);

    public static void main(String[] args) {
        List<String> parsedPdf = new PdfParser().parseFileByUrl(UtilsConfig.getUrlToPdf());

        // Get first char of each in Parsed PDF
        parsedPdf.stream().map(WorkWithLambda::getFirstChar).collect(Collectors.toList());
        // Replace digit in each for "*"
        parsedPdf.stream().forEach(s -> replaceDigitFor(s, "*"));
    }

    /**
     * Functional interfaces for lambda
     */
    interface LambdaOneArgument<T> {
        T method(T x);
    }

    /**
     * Return first char from word
     */

    private static String getFirstChar(String word) {
        LambdaOneArgument<String> getFirstChar = target -> target.substring(0, 1);
        String firstChar = getFirstChar.method(word);
        logger.info("First char of \"" + word + "\" is \"" + firstChar + "\"");
        return firstChar;
    }

    /**
     * Replace all digits for statement
     */

    private static String replaceDigitFor(String input, String replacement) {
        logger.info("Replace any digit for \"" + replacement + "\"");
        LambdaOneArgument<String> replaceDigit = word -> word.replaceAll("\\d", replacement);
        String result = replaceDigit.method(input);
        logger.info("Input: \"" + input + "\" -> output: \"" + result + "\"");
        return result;
    }
}


