package com.utils.parcer.parserBuilder;

import com.utils.parcer.Parser;

public interface ParserBuilder {
    Parser createParser();
}

