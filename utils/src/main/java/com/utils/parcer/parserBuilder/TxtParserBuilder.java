package com.utils.parcer.parserBuilder;

import com.utils.parcer.Parser;
import com.utils.parcer.TxtParser;

public class TxtParserBuilder implements ParserBuilder {
    public Parser createParser() {
        return new TxtParser();
    }
}
