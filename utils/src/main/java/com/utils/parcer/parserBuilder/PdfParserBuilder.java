package com.utils.parcer.parserBuilder;

import com.utils.parcer.Parser;
import com.utils.parcer.PdfParser;

public class PdfParserBuilder implements ParserBuilder {
    public Parser createParser() {
        return new PdfParser();
    }
}
