package com.utils.parcer.parserBuilder;

import com.utils.parcer.Parser;
import com.utils.parcer.XlsParser;

public class XlsParserBuider implements ParserBuilder {
    public Parser createParser() {
        return new XlsParser();
    }
}
