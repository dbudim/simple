package com.utils.parcer;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TxtParser implements Parser {

    private Logger logger = Logger.getLogger( TxtParser.class );

    @Override
    public List<String> parseFileByUrl( String url ) {
        List<String> dataFromFile = new ArrayList<String>();
        BufferedReader bufferedReader = null;
        try {
            logger.info( "Try to read file structure..." );
            bufferedReader = new BufferedReader( new InputStreamReader( new URL( url ).openStream() ) );
            String line = null;
            while ( ( line = bufferedReader.readLine() ) != null ) {
                dataFromFile.add( line );
            }
            logger.info( "Success! Received " + dataFromFile.size() + " rows!" );
        } catch ( IOException e ) {
            logger.error( "Can't read file structure from URL: " + url );
            e.printStackTrace();
            System.exit( 1 );
        } finally {
            try {
                bufferedReader.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
        return dataFromFile;
    }

    @Override
    public List<String> parseFileByPath( String pathToFile ) {
        List<String> dataFromFile = new ArrayList<String>();
        BufferedReader bufferedReader = null;
        try {
            logger.info( "Try to read file structure..." );
            bufferedReader = new BufferedReader( new FileReader( pathToFile ) );
            String line = null;
            while ( ( line = bufferedReader.readLine() ) != null ) {
                dataFromFile.add( line );
            }
            logger.info( "Success! Received " + dataFromFile.size() + " rows!" );
        } catch ( IOException e ) {
            logger.error( "Can't read file structure from: " + pathToFile );
            e.printStackTrace();
            System.exit( 1 );
        } finally {
            try {
                bufferedReader.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
        return dataFromFile;
    }
}
