package com.utils.parcer;

import java.util.List;

public interface Parser {

    public List<String> parseFileByUrl( String url );
    public List<String> parseFileByPath( String pathToFile );
}
