package com.utils.parcer;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class XlsParser implements Parser {

    Logger logger = Logger.getLogger(XlsParser.class);

    @Override
    public List<String> parseFileByUrl(String url) {
        List<String> dataFromFile = new ArrayList<String>();
        try {
            logger.info("Try to read file structure...");
            Workbook workbook = WorkbookFactory.create(new URL(url).openStream());
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();
            dataFromFile = new ArrayList<String>();
            for (Row row : sheet) {
                for (Cell cell : row) {
                    dataFromFile.add(dataFormatter.formatCellValue(cell));
                }
            }
            logger.info("Success! Received " + dataFromFile.size() + " rows!");
        } catch (IOException e) {
            logger.error("Can't read file structure from URL: " + url);
            e.printStackTrace();
            System.exit(1);
        }
        return dataFromFile;
    }

    @Override
    public List<String> parseFileByPath(String pathToFile) {
        List<String> dataFromFile = new ArrayList<String>();
        try {
            logger.info("Try to read file structure...");
            Workbook workbook = WorkbookFactory.create(new File(pathToFile));
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();
            for (Row row : sheet) {
                for (Cell cell : row) {
                    dataFromFile.add(dataFormatter.formatCellValue(cell));
                }
            }
            logger.info("Success! Received " + dataFromFile.size() + " rows!");
        } catch (IOException e) {
            logger.error("Can't read file structure from: " + pathToFile);
            e.printStackTrace();
            System.exit(1);
        }
        return dataFromFile;
    }

    public List<String> getColumnValuesFromDocumnet(String urlToFile, int columnIndex) {
        List<String> columnData = new ArrayList<>();
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new URL(urlToFile).openStream());
        } catch (IOException e) {
            logger.error("Can't get file by URL " + urlToFile);
        }
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            try {
                columnData.add(sheet.getRow(i).getCell(columnIndex).toString());
            } catch (NullPointerException e) {
                logger.error("Received blank cell in " + i + " row" + " of " + columnIndex + " column");
            }
        }
        return columnData;
    }

    public List<String> getCellsOfRowInDocument(String urlToFile, int rowIndex) {
        List<String> cells = new ArrayList<>();
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new URL(urlToFile).openStream());
        } catch (IOException e) {
            logger.error("Can't get file from URL " + urlToFile);
        }
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            try {
                cells.add(sheet.getRow(rowIndex).getCell(i).toString());
            } catch (NullPointerException e) {
                logger.error("Received blank cell in " + rowIndex + " row" + " of " + i + " column");
            }
        }
        return cells;
    }

    public int getIndexOfColumnByName(String urlToFile, String columnName) {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new URL(urlToFile).openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheetAt(0);
        int columnIndex = 0;
        for (int i = 0; i <= sheet.getRow(0).getLastCellNum(); i++) {
            try {
                Cell cell = sheet.getRow(0).getCell(i);
                if (cell.toString().equals(columnName)) {
                    columnIndex = i;
                }
            } catch (NullPointerException e) {
                logger.error("Blank cell in column " + i);
            }
        }
        return columnIndex;
    }


}

