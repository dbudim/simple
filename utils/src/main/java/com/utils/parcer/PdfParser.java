package com.utils.parcer;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class PdfParser implements Parser {

    private Logger logger = Logger.getLogger( PdfParser.class );

    @Override
    public List<String> parseFileByUrl( String url ) {
        List<String> dataFromFile = null;
        PDDocument document = null;
        try {
            logger.info( "Try to read file structure..." );
            document = PDDocument.load( new URL( url ).openStream() );
            dataFromFile = Arrays.asList( new PDFTextStripper().getText( document )
                                                               .split( "\\r?\\n" ) );
            logger.info( "Success! Received " + dataFromFile.size() + " rows!" );
        } catch ( IOException e ) {
            logger.error( "Can't read file structure from URL: " + url );
            e.printStackTrace();
            System.exit( 1 );
        } finally {
            try {
                document.close();
            } catch ( IOException e ) {
                logger.error( "Can't close document!" );
                e.printStackTrace();
            }
        }
        return dataFromFile;
    }

    @Override
    public List<String> parseFileByPath( String pathToFile ) {
        List<String> dataFromFile = null;
        PDDocument document = null;
        try {
            logger.info( "Try to read file structure..." );
            document = PDDocument.load( new File( pathToFile ) );
            dataFromFile = Arrays.asList( new PDFTextStripper().getText( document )
                                                               .split( "\\r?\\n" ) );
            logger.info( "Success! Received " + dataFromFile.size() + " rows!" );
        } catch ( IOException e ) {
            logger.error( "Can't read file structure from: " + pathToFile );
            e.printStackTrace();
            System.exit( 1 );
        } finally {
            try {
                document.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
        return dataFromFile;
    }

}
